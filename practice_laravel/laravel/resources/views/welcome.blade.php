<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel | Asbath Fadil Nurhidayat</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600;700&display=swap');

        html,
        body {
            background-color: #212529;
            /* color: #636b6f; */
            font-family: 'Poppins', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            overflow-x: hidden;
        }

        .decoration {
            width: 660px;
            height: 660px;
            background-color: #1c1f22;
            border-radius: 1000px;
            position: absolute;
            /* z-index: -1; */

            animation: ball 20s forwards;
        }

        @keyframes ball {
            0% {
                left: 0;
            }

            /* 50%{
                left:400px;
            } */

            100% {
                left:683px;
                background-color: #212529;
            }
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            color: red;
            font-size: 34px;
            font-weight: 500;
            position: relative;
            z-index: 99;
            animation: title 62s forwards ;
            margin-top: 150px;
        }

        @keyframes title{
            0%{
                opacity: 0;
            }
            100{
                opacity:1;
            }
        }

        .links>a {
            animation: title 62s forwards ;
            position: relative;
            z-index: 99;
            color: red;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        @media (min-width:1023px){
            .title {
            color: red;
            font-size: 84px;
        }

    </style>

</head>

<body>
        <div class="content">
            <div class="title m-b-md">
                ASBATH FADIL NURHIDAYAT
            </div>

            <div class="links">
                <a href="https://laravel.com/docs">Docs</a>
                <a href="https://laracasts.com">Laracasts</a>
                <a href="https://laravel-news.com">News</a>
                <a href="https://blog.laravel.com">Blog</a>
                <a href="https://nova.laravel.com">Nova</a>
                <a href="https://forge.laravel.com">Forge</a>
                <a href="https://vapor.laravel.com">Vapor</a>
                <a href="https://github.com/laravel/laravel">GitHub</a>
            </div>
        </div>
    </div>
</body>

</html>
