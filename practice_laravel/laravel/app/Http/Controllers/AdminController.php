<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //login home admin
    public function index()
    {
        return view('login.login');
    }


    // validasi login admin
    public function process(Request $request)
    {
        $validasiData = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $result = Admin::where('username', '=', $validasiData['username'])->first();
        if ($result) {
            if ($request->password == $result->password) {
                session(['username' => $request->password]);
                return redirect('/student');
            } else {
                return back()->withInput()->with('pesan', 'Login gagal');
            }
        } else {
            return back()->withInput()->with('pesan', 'Login gagal');
        }
    }


    // logout admin
    public function logout()
    {
        session()->forget('username');
        return redirect('login')->with('pesan', 'Logout Berhasil');
    }
}
